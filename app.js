'use strict';

/**
 * Author: bo.liu@adara.com
 *
 * Send email through SendGrid service upon receiving POST request from Pub/Sub
 *
 * Pub/Sub Info
 *  subscription: projects/adara-compass-dev/subscriptions/app-engine-email
 *  topic: projects/adara-compass-dev/topics/email
 *  push to endpoint: https://adara-compass-dev.appspot.com/
 *
 * */

const express = require('express');
const sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
const assert = require('assert');
const app = express();


/**
 * Receive POST request from Pub/Sub and send email
 * */
app.post('/', (req, res) => {

    let body = [];

    req.on('error', (err) => {
        console.error(err);

    }).on('data', (chunk) => {
        body.push(chunk);

    }).on('end', () => {
        // Body of POST request sent from Pub/Sub
        // Message data is base64 encoded
        let decodedBody = JSON.parse(Buffer.concat(body).toString());
        let messageDataArray = new Buffer(decodedBody['message']['data'], 'base64').toString().split('|');

        assert(messageDataArray.length, 3);

        let emailFromAddress = 'app-engine@adara.com';
        let emailFromName = 'App Engine';
        let emailToAddress = messageDataArray[0];
        let subject = messageDataArray[1];
        let content = messageDataArray[2];

        sendEmail(emailFromAddress, emailFromName, emailToAddress, subject, content);

    });

    res.status(200)
        .send('Hello, world!')
        .end();
});

app.get('/', (req, res) => {
    res.status(200)
        .send('Hello, world!')
        .end();
});

/**
 * Send email through SendGrid
 * */
const sendEmail = (emailFromAddress, emailFromName, emailToAddress, subject, emailContent) => {
    const helper = require('sendgrid').mail;

    const fromEmail = new helper.Email(emailFromAddress, emailFromName);
    const toEmail = new helper.Email(emailToAddress);
    const content = new helper.Content('text/plain', emailContent);
    const mail = new helper.Mail(fromEmail, subject, toEmail, content);


    console.log('Email From: ', emailFromAddress);
    console.log('Email To: ', emailToAddress);
    console.log('Email Subject: ', subject);
    console.log('Email Content: ', emailContent);

    const request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, (error, response) => {
        if (error) {
            console.log('Error response received');
        }
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    });
};

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
    console.log('Press Ctrl+C to quit.');
});