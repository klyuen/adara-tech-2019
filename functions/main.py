import os
import sendgrid
from sendgrid.helpers.mail import Email, Content, Mail

project_id = os.environ.get("PROJECT_ID")


def read_input_attr(data):
    """
    Parse data dictionary for message and 'key' attribute
    :param data:
    :return: (string, object)
    """
    import base64
    msg = None
    if 'data' in data:
        msg = base64.b64decode(data['data']).decode('utf-8')
    if 'attributes' in data:
        attrs = data["attributes"]
    else:
        raise KeyError("Missing 'attributes' in message")
    return msg, attrs


def write_less_code(data, context):
    # read input variables
    msg, attrs = read_input_attr(data)
    parsed_msg = msg.split('|')

    # build email header
    recipient_email = parsed_msg[0]
    me = 'Cloud Function <cloud-function@adara.com>'
    subject = parsed_msg[1]
    from_email = Email(me)
    to_email = Email(recipient_email)
    mail = Mail(from_email, subject, to_email)

    # construct message body
    text = parsed_msg[2]
    text_part = Content("text/plain", text)
    mail.add_content(text_part)

    sg = sendgrid.SendGridAPIClient()
    mail_data = mail.get()
    # send email to user
    response = sg.client.mail.send.post(request_body=mail_data)
