###Dev
```bash
brew install go
```

###Build
```bash
gcloud builds submit --tag gcr.io/adara-compass-dev/cloud-run-email
```

###Deploy
```bash
gcloud beta run deploy cloud-run-email --image gcr.io/adara-compass-dev/cloud-run-email
```

###Test
```bash
gcloud pubsub topics publish email --message "shuo.yang@adara.com|Reminder: Order Lunch|Hello, this is a daily reminder to order your eat club lunch"
```

set environment variables
```bash
gcloud beta run deploy cloud-run-email --image gcr.io/adara-compass-dev/cloud-run-email \
--set-env-vars=SENDGRID_API_KEY=__THE_REAL_KEY__
```

###Pub/Sub setups

```bash
gcloud projects add-iam-policy-binding adara-compass-dev \
     --member=serviceAccount:service-345391081548@gcp-sa-pubsub.iam.gserviceaccount.com \
     --role=roles/iam.serviceAccountTokenCreator

gcloud iam service-accounts create cloud-run-pubsub-invoker \
     --display-name "Cloud Run Pub/Sub Invoker"


gcloud beta run services add-iam-policy-binding cloud-run-email \
     --member=serviceAccount:cloud-run-pubsub-invoker@adara-compass-dev.iam.gserviceaccount.com \
     --role=roles/run.invoker

gcloud beta pubsub subscriptions create cloud-run-email --topic mail \
   --push-endpoint=https://cloud-run-email-32lvfh7eea-uc.a.run.app \
   --push-auth-service-account=cloud-run-pubsub-invoker@adara-compass-dev.iam.gserviceaccount.com
```
