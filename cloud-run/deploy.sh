#!/usr/bin/env bash
gcloud config set project adara-compass-dev

gcloud builds submit --tag gcr.io/adara-compass-dev/cloud-run-email

gcloud beta run deploy cloud-run-email --image gcr.io/adara-compass-dev/cloud-run-email